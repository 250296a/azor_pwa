$(document).ready(function () {
  var swiper = new Swiper(".main-banner", {
    effect: "fade",
    autoplay: {
      delay: 4000,
      disableOnInteraction: false,
    }
  });
  var swiper = new Swiper(".brands-slider", {
    speed: 10000,
    loop: true,
    autoplay: {
      delay: 7000,
      disableOnInteraction: false,
    },
    breakpoints: {
      0: {
        slidesPerView: 5
      },
      430: {
        slidesPerView: 6
      }
    }
  });
  var swiper = new Swiper(".popular-brands-slider", {
    spaceBetween: 10,
    speed: 10000,
    loop: true,
    autoplay: {
      delay: 7000,
      disableOnInteraction: false,
    },
    breakpoints: {
      0: {
        slidesPerView: 3
      },
      350: {
        slidesPerView: 4
      }
    }
  });

  var swiper = new Swiper(".adv-slider1", {
    speed: 1500,
    loop: true,
    autoplay: {
      delay: 15000,
      disableOnInteraction: false,
    }
  });
  var swiper = new Swiper(".adv-slider2", {
    speed: 1500,
    loop: true,
    autoplay: {
      delay: 16500,
      disableOnInteraction: false,
    }
  });

  var swiper = new Swiper(".category-slider ", {
    slidesPerView: 4,
    spaceBetween: 5,
    speed: 1500,
    loop: true,
    autoplay: {
      delay: 10500,
      disableOnInteraction: false,
    },
    breakpoints: {
      0: {
        slidesPerView: 3
      },
      350: {
        slidesPerView: 4
      }
    }
  });

  var swiper = new Swiper(".double-slider-js", {
    slidesPerView: 2,
    spaceBetween: 15,
    speed: 1500
  });
  var swiper = new Swiper(".triple-slider-js", {
    slidesPerView: 3,
    spaceBetween: 15,
    speed: 1500,
    breakpoints: {
      0: {
        slidesPerView: 2
      },
      361: {
        slidesPerView: 3
      }
    }
  });


  var swiper = new Swiper(".slider-js", {
    slidesPerView: 1,
    centeredSlides: true,
    loop: true,
    speed: 500,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
  });

  var heightSlider = $('.slider-js .swiper-slide-active').height() + 10;
  $('.slider').css('height', heightSlider);


  $('.select__selected').on('click', function () {
    $(this).next().toggle();
    $('.select__selected').not(this).next().hide();
  });
  $('.select__option').on('click', function () {
    var option = $(this).attr('data-val');
    $(this).html(option);
    $(this).parent().parent().find('.select__selected span').html(option);
    $(this).parent().parent().find('.select__input').val(option);
  });

  jQuery(function ($) {
    $(document).mouseup(function (e) {
      var div = $(".select__selected");
      if (!div.is(e.target) &&
        div.has(e.target).length === 0) {
        $('.select__dropdown').hide();
      }
    });
  });

  $('.minus').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $('.plus').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });

  (function ($) {
    $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');

    $('.tab ul.tabs li .bt').click(function () {
      var tab = $(this).closest('.tab'),
        index = $(this).closest('li').index();

      tab.find('ul.tabs > li').removeClass('current');
      $(this).closest('li').addClass('current');

      tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
      tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();


    });
  })(jQuery);

  $('.order__pay-method-item').on('click', function () {
    $(this).toggleClass('--open');
    $('.order__pay-method-item').not(this).removeClass('--open');
  });
  jQuery(function ($) {
    $(document).mouseup(function (e) {
      var div = $(".order__pay-method-item");
      if (!div.is(e.target) &&
        div.has(e.target).length === 0) {
        $('.order__pay-method-item').removeClass('--open');
      }
    });
  });

  $('.detail-js').on('click', function () {
    $('.product-detail').addClass('--open');
    $('body').addClass('no-scroll');
  });
  $('.product-detail__close').on('click', function () {
    $('.product-detail').removeClass('--open');
    $('body').removeClass('no-scroll');
  });
  $('.profile__name-settings').on('click', function () {
    $('.profile__name-settings').hide();
    var input = $(".profile__name input");
    var len = input.val().length;
    input[0].focus();
    input[0].setSelectionRange(len, len);
    $('.profile__name input').addClass('--change');
  });

  jQuery(function ($) {
    $(document).mouseup(function (e) {
      var div = $(".profile__name input");
      if (!div.is(e.target) &&
        div.has(e.target).length === 0) {
        $('.profile__name-settings').show();
        $('.profile__name input').removeClass('--change');

        if (!$('.profile__name input').val()) {
          $('.name-js').addClass('--error');
          $('.profile__name-settings').hide();
          $('.profile__name input').focus();
        } else {
          $('.name-js').removeClass('--error');
        }
      }
    });
  });





  $(".profile__input input").change(function () {
    if (!$(this).val()) {
      $(this).parent().addClass('--error');
      $(this).focus();
    } else {
      $(this).parent().removeClass('--error');
    }
  });


  $('.characteristics-js').on('click', function () {
    $(this).toggleClass('--open');
    $('.characteristics-js').not(this).removeClass('--open');
  });
  $('.comparison__characteristics-option').on('click', function () {
    var option = $(this).attr('data-val');
    $(this).parent().parent().find('input').val(option);
    $(this).parent().parent().find('.comparison__characteristics-selected').html(option);
  });

  jQuery(function ($) {
    $(document).mouseup(function (e) {
      var div = $(".characteristics-js");
      if (!div.is(e.target) &&
        div.has(e.target).length === 0) {
        $('.characteristics-js').removeClass('--open');
      }
    });
  });



  $('.additional-desc__more').on('click', function () {
    $(this).prev().addClass('--open');
    $(this).hide();
    $('.additional-desc__hide').show();
  });
  $('.additional-desc__hide').on('click', function () {
    $('.additional-desc__more').show();
    $(this).prev().prev().removeClass('--open');
    $(this).hide();
  });

  $('.hamburger').on('click', function () {
    $('body').addClass('no-scroll');
    $('.menu').addClass('--open');
  });
  $('.menu__close').on('click', function () {
    $('body').removeClass('no-scroll');
    $('.menu').removeClass('--open');
  });












});